#include "opencv2/opencv.hpp"
#include <stdlib.h>
#include <sys/time.h>
#include <cpr/cpr.h>
#include <string>
#include <fstream>
#include <streambuf>

//#include <opencv2/highgui/highgui.hpp>


#define STATE  "http://localhost.dti.supsi.ch:5000/worlds/Test/map/states/L2S2";

#if TYPE == 0
#include "crowddensity.h"
#define MODULE_NAME "Crowd density"
#define CONS createCrowdDensity
#define DEST destroyCrowdDensity
#define PROC processCrowdDensity
#define NAME "number_of_people_xml"
#define SEND_PERIOD 1000
#define RUN_PERIOD 1000

#elif TYPE == 1
#include "wheelchairtracker.h"
#define MODULE_NAME "wheelchair tracker"
#define CONS createWheelchairTracker
#define DEST destroyWheelchairTracker
#define PROC processWheelchairTracker
#define NAME "people_xml"
#define SEND_PERIOD 1000
#define RUN_PERIOD 0

void set_calib()
{
  std::ifstream t("calib.xml");
  std::stringstream buffer;
  buffer << t.rdbuf();
  std::string s = buffer.str();
  const char *xml = s.c_str();
  bool success = setWheelchairTrackerCalibration(xml);
  if(success)
  {
    printf("Camera calibration set.\n");
  }
  else
  {
    printf("Unable to set camera calibration\n");
    exit(-1);
  }
}

#else

#error TYPE not defined

#endif




int64_t current_timestamp() {
        struct timeval te;
        gettimeofday(&te, NULL);
        long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000;
        return milliseconds;
}

void send_data(cpr::Url url, const char *data, float timeout_s)
{
        long timeout_ms = long(timeout_s * 1000);
        auto r = cpr::Post(url, cpr::Body {data}, cpr::Header {{"Content-Type", "application/json"}}); //, cpr::Timeout{timeout_ms});
        std::cout << r.status_code <<std::endl;
}

void usage(char *p)
{
        printf("Usage: %s  [-r <run period>] [-s <send period>] -n <attribute name> -u <state url>  -i <input stream>\n", p);
        exit(-1);
}

int main(int argc, char *argv[])
{
        std::string input = "?";
        std::string name = NAME;
        std::string state_uri = STATE;
        int send_period = SEND_PERIOD;
        int run_period = RUN_PERIOD;
        char *p = argv[0];
        for (int i = 1; i < argc-1; i+=2) {
                if(strcmp(argv[i],"-h")==0)
                {
                        usage(p);
                }
                else if(strcmp(argv[i], "-i")==0)
                {
                        input = std::string(argv[i+1]);
                }
                else if(strcmp(argv[i], "-n")==0)
                {
                        name = std::string(argv[i+1]);
                }
                else if(strcmp(argv[i], "-u")==0)
                {
                        state_uri = std::string(argv[i+1]);
                }
                else if(strcmp(argv[i], "-r")==0)
                {
                        if(sscanf(argv[i+1],"%d",&run_period)!=1)
                        {
                                usage(p);
                        }
                }
                else if(strcmp(argv[i], "-s")==0)
                {
                        if(sscanf(argv[i+1],"%d",&send_period)!=1)
                        {
                                usage(p);
                        }
                }
                else
                {
                        usage(p);
                }
        }

        cv::VideoCapture capture(input);
        if (!capture.isOpened())
        {
                std::cerr << "failed to open the input video "<< input << std::endl;
                exit(-1);
        }
        else
        {
                std::cout << "Opened video stream " << input << std::endl;
        }

        CONS();
        #if TYPE == 1
          set_calib();
        #endif
        std::cout << MODULE_NAME <<" module created." << std::endl;

        std::cout << "It will run every " << run_period << " ms ";

        cpr::Url url = cpr::Url {state_uri+"/"+name};

        std::cout << "and send the data to " << state_uri+"/"+name <<" every " << send_period << " ms." << std::endl;

        uint64_t frame_number = 0;
        cv::Mat frame;

        char output_buffer[5000];
        char prev_buffer[5000];
        char json_buffer[6000];
        int32_t bytes_written;


        //double fps = capture.get(CV_CAP_PROP_FPS);
        long ts = current_timestamp();
        long run_ts = ts + run_period;
        long send_ts = ts + send_period;
        //printf("%d at %d\n",frame_number,ts);
        //long i_ts = ts;
        while(capture.grab())
        {
                ts = current_timestamp();
                frame_number++;

                //printf("%d fps\n",(1000*frame_number)/ (ts-i_ts));
                {
                        capture.retrieve(frame);
                        //cv::imshow( "Display window", frame );
                        //cv::waitKey(1);
                        uint8_t *image_data = frame.data;
                        PROC(image_data, frame.cols, frame.rows, ALMA_IMAGE_BGR_888,
                             current_timestamp(), frame_number, output_buffer, 5000,
                             &bytes_written);
                        run_ts= ts + run_period;
                        printf(output_buffer);
                        if (ts > send_ts) {
                                if(strcmp(prev_buffer, output_buffer)!=0)
                                {
                                        strcpy(prev_buffer,output_buffer);
                                        output_buffer[bytes_written-2]='\0';
                                        sprintf(json_buffer, "{\"value\": {\"xml\":\"%s\"}}", output_buffer);
                                        printf("%ld sending: %s\n",ts,json_buffer);
                                        send_data(url, json_buffer, 0);
                                        send_ts = ts + send_period;
                                }
                        }
                }
        }
        //printf("%d at %d\n",frame_number,ts);
        DEST();
        return 0;
}
