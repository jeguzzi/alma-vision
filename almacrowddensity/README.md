# Crowd Denstiy Estimation Application

## Introduction

This is a command line interface application that estimates the crowd density from a video stream and prints out the estimated number of people in the video.

## Building

Building almacrowddensity requires OpenCV. The latest version of OpenCV can be acquired from [its webpage](http://www.opencv.org/downloads.html).

After installing OpenCV, modify the Makefile's INC list to include where OpenCV header files are located.

Then run:

'''
make all
'''

## Usage

Example of using a video file as an input:

'''
almacrowddensity ./Videos/testvid.avi
'''

Example of using an RTSP stream as an input:

'''
almacrowddensity rtsp://192.168.1.231:554/ufirststream
'''
