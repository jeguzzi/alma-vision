#pragma once

#include "almatypes.h"
#include <stdint.h>
#include <stdbool.h>

/** @file "crowddensity.h"
 *  @brief The crowd density module API header
 *
 *  This file defines the public interface for the crowd density estimation module
 *
 */
#ifdef __cplusplus
extern "C"
{
#endif

/** @brief Initialises the Crowd Density estimation module
 *
 *  The createCrowdDensity() function may optionally be called to initialise 
 *  the crowd density module.  If it is not called explicitly, the module will
 *  be initialised with the first call to any of the other functions declared
 *  in this header.
 *
 *  When the crowd density module is no longer required, the destroyCrowdDensity() fuction should be called to release the resources that were allocated.
 */
VCA_ALMA_EXPORT_FROM_DLL void createCrowdDensity();

/** @brief Processes one frame of video
 *  
 *  The processCrowdDensity() function must be called to supply the module with
 *  each frame of video to be processed.  The crowd density estimate will be 
 *  written to the memory that outputBuffer points to and will conform to the
 *  following schema:
 *
 *  @code
 *	<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
 *	  <xs:element name="crowddensity" type="xs:int"/>
 *  </xs:schema>i
 *  @endcode
 *
 *  @param[in] imageData          A pointer to the raw image data to be processed.  The data must be valid for the duration of the function call only.
 *  @param[in] imageWidth         The width of the image in pixels
 *  @param[in] imageHeight        The height of the image in pixels
 *  @param[in] imageFormat        The format of the image data
 *  @param[in] timestampMicros    The frame timestamp, measured in microseconds.  This can be measured from the start of the video, from the epoch, or any other fixed point of reference.
 *  @param[in] frameNumber        A sequence number for the frame.  This should be 0 for the first frame that is supplied, 1 for the second etc.
 *  @param[out] outputBuffer      A pointer to a buffer where the output is to be written
 *  @param[in] outputBufferSize   The maximum number of bytes that may be written to the output buffer.  This will normally be the size of the buffer that outputBuffer points to.
 *  @param[out] bytesWritten      The number of bytes that were written to outputBuffer.  The value will always be written to in a call to processCrowdDensity.
 *
 *  @returns                      True if the frame was processed successfully, false otherwise.
 */
VCA_ALMA_EXPORT_FROM_DLL bool processCrowdDensity(const uint8_t* imageData, int32_t imageWidth, int32_t imageHeight, AlmaImageFormat imageFormat, int64_t timestampMicros, int64_t frameNumber, char* outputBuffer, int32_t outputBufferSize, int32_t* bytesWritten);

/** @brief Obtains the version information for the module
 *  @returns                      A null-terminated string describing the module version, e.g. "1.0"
 */
VCA_ALMA_EXPORT_FROM_DLL const char * getCrowdDensityRevision();

/** @brief Sets the calibration data for the crowd density module
 *
 * The calibration data is supplied as an XML string containing tags for each of
 * the calibration values.  The format should resemble that of the example .vcb
 * calibration files, which is defined in the schema file <a href="../schemas/calibrationschema.xsd">calibrationschema.xsd</a>:
 *
 * @code
 * <xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
 *   <xs:element name="calibration">
 *     <xs:complexType>
 *       <xs:element name="rotation">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *           <xs:element type="xs:float" name="z"/>
 *           <xs:element type="xs:float" name="w"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="translation">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *           <xs:element type="xs:float" name="z"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="principalPoint">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="focalLength">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="distortion">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="k1"/>
 *           <xs:element type="xs:float" name="k2"/>
 *           <xs:element type="xs:float" name="k3"/>
 *           <xs:element type="xs:float" name="k4"/>
 *           <xs:element type="xs:float" name="k5"/>
 *           <xs:element type="xs:float" name="k6"/>
 *           <xs:element type="xs:float" name="p1"/>
 *           <xs:element type="xs:float" name="p2"/>
 *         </xs:complexType>
 *       </xs:element>
 *     </xs:complexType>
 *   </xs:element>
 * </xs:schema>
 * @endcode
 * 
 * The elements have the following meanings:
 *
 * - <b>rotation:</b> The orientation of the camera, specified as a quaternion.  The rotation is specified relative to the world coordinates, in which z points upwards and x and y lie in the ground plane in a right-handed coordinate system.  In the camera's frame of reference, x and y specify 2D image coordinates and the z points away from the camera.
 * - <b>translation:</b> The translation of the camera centre from the origin.  The vector is specified in world coordinates and has units of metres.
 * - <b>principalPoint:</b> The 2D image pont corresponding to the principal ray of the camera.  The point is specified as a fraction of the image width and height.
 * - <b>focalLength:</b> The focal length of the camera, measured in multiples of the image width and height.
 * - <b>distortion:</b> Coefficients for modelling radial and tangential lens distortion.  
 *
 * @param calibrationData         A null-terminated string containing the calibration data in XML format
 */
VCA_ALMA_EXPORT_FROM_DLL bool setCrowdDensityCalibration(const char * calibrationData);

/** @brief Resets the crowd density estimation module
 *
 *  The resetCrowdDensity function should be called if the video source or
 *  format changes.
 */
VCA_ALMA_EXPORT_FROM_DLL void resetCrowdDensity();

/** @brief Destroys the data associated with the module
 *
 *  The destroyCrowdDensity function should be called when the crowd density
 *  module is no longer required.  All temporary data will be destroyed and the
 *  associated memory will be released.
 */
VCA_ALMA_EXPORT_FROM_DLL void destroyCrowdDensity();

#ifdef __cplusplus
}
#endif

/** @mainpage Crowd Density Estimate Module Documentation
 *
 *  \section Introduction
 *
 *  This is the documentation for the ALMA crowd density estimation module (D3.2).  Details on the implemented algorithm and expected behaviour can be found in the D3.2 report.
 *
 *  \section Usage
 *
 *  The module is packaged as a shared library which is intended to run under 32-bit linux.  The basic usage is:
 *
 *  -# Initialise the module by calling createCrowdDensity().
 *  -# If calibration data is available for the camera view, call setCrowdDensityCalibration() so that the module can use it.
 *  -# Process video data - for every frame of video, processCrowdDensity() should be called to provide the module with the video data and to retrieve the latest estimates.
 *  -# When there is no more processing to be done, release the module resources by calling destroyCrowdDensity().
 */

