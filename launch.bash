#!/usr/bin/env bash

export STATE=http://192.168.1.114:5000/worlds/ISC/map/states/L3S1
export CAMERA=rtsp:rtsp://192.168.189.3:554/ufirststream


cd $PWD/install/bin/alma-tracker; ./alma-tracker -n tracked_targets -u $STATE -i $CAMERA &
cd $PWD/install/bin/alma-crowd; ./alma-crowd -n crowd_count -u $STATE -i $CAMERA &
