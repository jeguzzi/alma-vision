#pragma once

#include "almatypes.h"
#include <stdint.h>
#include <stdbool.h>

/** @file "wheelchairtracker.h"
 *  @brief The wheelchair tracker module API header
 *
 *  This file defines the public interface for the wheelchair tracker estimation module
 *
 */
#ifdef __cplusplus
extern "C"
{
#endif

/** @brief Initialises the Wheelchair Tracker estimation module
 *
 *  The createWheelchairTracker() function may optionally be called to initialise 
 *  the wheelchair tracker module.  If it is not called explicitly, the module will
 *  be initialised with the first call to any of the other functions declared
 *  in this header.
 *
 *  When the wheelchair tracker module is no longer required, the 
 *  destroyWheelchairTracker() fuction should be called to release the resources
 *  that were allocated.
 */
VCA_ALMA_EXPORT_FROM_DLL void createWheelchairTracker();

/** @brief Processes one frame of video
 *  
 *  The processWheelchairTracker() function must be called to supply the module
 *  with each frame of video to be processed.  The wheelchair tracker output
 *  will be written to the memory that outputBuffer points to and will conform
 *  to the following schema:
 *
 *  @code
 *  <xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
 *    <xs:element name="trackedtargets">
 *      <xs:complexType>
 *        <xs:element name="target" maxOccurs="unbounded" minOccurs="0">
 *          <xs:complexType>
 *            <xs:element type="xs:int" name="id"/>
 *            <xs:element type="xs:float" name="left"/>
 *            <xs:element type="xs:float" name="top"/>
 *            <xs:element type="xs:float" name="right"/>
 *            <xs:element type="xs:float" name="bottom"/>
 *            <xs:element type="xs:string" name="class"/>
 *          </xs:complexType>
 *        </xs:element>
 *      </xs:complexType>
 *    </xs:element>
 *  </xs:schema>
 *  @endcode
 *
 *  @param[in] imageData          A pointer to the raw image data to be processed.  The data must be valid for the duration of the function call only.
 *  @param[in] imageWidth         The width of the image in pixels
 *  @param[in] imageHeight        The height of the image in pixels
 *  @param[in] imageFormat        The format of the image data
 *  @param[in] timestampMicros    The frame timestamp, measured in microseconds.  This can be measured from the start of the video, from the epoch, or any other fixed point of reference.
 *  @param[in] frameNumber        A sequence number for the frame.  This should be 0 for the first frame that is supplied, 1 for the second etc.
 *  @param[out] outputBuffer      A pointer to a buffer where the output is to be written
 *  @param[in] outputBufferSize   The maximum number of bytes that may be written to the output buffer.  This will normally be the size of the buffer that outputBuffer points to.
 *  @param[out] bytesWritten      The number of bytes that were written to outputBuffer.  The value will always be written to in a call to processWheelchairTracker.
 *
 *  @returns                      True if the frame was processed successfully, false otherwise.
 */
VCA_ALMA_EXPORT_FROM_DLL bool processWheelchairTracker(const uint8_t* imageData, int32_t imageWidth, int32_t imageHeight, AlmaImageFormat imageFormat, int64_t timestampMicros, int64_t frameNumber, char* outputBuffer, int32_t outputBufferSize, int32_t* bytesWritten);

/** @brief Obtains the version information for the module
 *  @returns                      A null-terminated string describing the module version, e.g. "1.0"
 */
VCA_ALMA_EXPORT_FROM_DLL const char * getWheelchairTrackerRevision();

/** @brief Sets the calibration data for the wheelchair tracker module
 *
 * The calibration data is supplied as an XML string containing tags for each of
 * the calibration values.  The format should resemble that of the example .vcb
 * calibration files, which is defined in the schema file <a href="../schemas/calibrationschema.xsd">calibrationschema.xsd</a>:
 *
 * @code
 * <xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
 *   <xs:element name="calibration">
 *     <xs:complexType>
 *       <xs:element name="rotation">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *           <xs:element type="xs:float" name="z"/>
 *           <xs:element type="xs:float" name="w"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="translation">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *           <xs:element type="xs:float" name="z"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="principalPoint">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="focalLength">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="x"/>
 *           <xs:element type="xs:float" name="y"/>
 *         </xs:complexType>
 *       </xs:element>
 *       <xs:element name="distortion">
 *         <xs:complexType>
 *           <xs:element type="xs:float" name="k1"/>
 *           <xs:element type="xs:float" name="k2"/>
 *           <xs:element type="xs:float" name="k3"/>
 *           <xs:element type="xs:float" name="k4"/>
 *           <xs:element type="xs:float" name="k5"/>
 *           <xs:element type="xs:float" name="k6"/>
 *           <xs:element type="xs:float" name="p1"/>
 *           <xs:element type="xs:float" name="p2"/>
 *         </xs:complexType>
 *       </xs:element>
 *     </xs:complexType>
 *   </xs:element>
 * </xs:schema>
 * @endcode
 * 
 * The elements have the following meanings:
 *
 * - <b>rotation:</b> The orientation of the camera, specified as a quaternion.  The rotation is specified relative to the world coordinates, in which z points upwards and x and y lie in the ground plane in a right-handed coordinate system.  In the camera's frame of reference, x and y specify 2D image coordinates and the z points away from the camera.
 * - <b>translation:</b> The translation of the camera centre from the origin.  The vector is specified in world coordinates and has units of metres.
 * - <b>principalPoint:</b> The 2D image pont corresponding to the principal ray of the camera.  The point is specified as a fraction of the image width and height.
 * - <b>focalLength:</b> The focal length of the camera, measured in multiples of the image width and height.
 * - <b>distortion:</b> Coefficients for modelling radial and tangential lens distortion.  
 *
 * @param calibrationData         A null-terminated string containing the calibration data in XML format
 */
VCA_ALMA_EXPORT_FROM_DLL bool setWheelchairTrackerCalibration(const char * calibrationData);

/** @brief Resets the wheelchair tracker estimation module
 *
 *  The resetWheelchairTracker function should be called if the video source or
 *  format changes.
 */
VCA_ALMA_EXPORT_FROM_DLL void resetWheelchairTracker();

/** @brief Destroys the data associated with the module
 *
 *  The destroyWheelchairTracker function should be called when the wheelchair
 *  tracker module is no longer required.  All temporary data will be destroyed
 *  and the associated memory will be released.
 */
VCA_ALMA_EXPORT_FROM_DLL void destroyWheelchairTracker();

#ifdef __cplusplus
}
#endif

/** @mainpage Wheelchair Tracker Module Documentation
 *
 *  \section Introduction
 *
 *  This is the documentation for the ALMA wheelchair tracker module (D3.1).  Details on the implemented algorithm and expected behaviour can be found in the D3.1 report.
 *
 *  \section Usage
 *
 *  The module is packaged as a shared library which is intended to run under 32-bit linux.  The basic usage is:
 *
 *  -# Initialise the module by calling createWheelchairTracker().
 *  -# If calibration data is available for the camera view, call setWheelchairTrackerCalibration() so that the module can use it.
 *  -# Process video data - for every frame of video, processWheelchairTracker() should be called to provide the module with the video data and to retrieve the latest estimates.
 *  -# When there is no more processing to be done, release the module resources by calling destroyWheelchairTracker().
 */

