#pragma once

/** @file "almatypes.h"
 *  @brief Types defined for the alma project interfaces
 *
 *  This file contains types, structures and macros that are common to VCA ALMA interfaces
 */

#if defined _WIN32 || defined __CYGWIN__
#   ifdef BUILDING_DLL
#       ifdef __GNUC__
#           define VCA_ALMA_EXPORT_FROM_DLL __attribute__((dllexport))
#           define VCA_ALMA_EXPORT_FROM_APP __attribute__((dllimport))
#       else
#           define VCA_ALMA_EXPORT_FROM_DLL __declspec(dllexport)
#           define VCA_ALMA_EXPORT_FROM_APP __declspec(dllimport)
#       endif
#   else
#       ifdef __GNUC__
#           define VCA_ALMA_EXPORT_FROM_DLL __attribute__((dllimport))
#           define VCA_ALMA_EXPORT_FROM_APP __attribute__((dllexport))
#       else
#           define VCA_ALMA_EXPORT_FROM_DLL __declspec(dllimport)
#           define VCA_ALMA_EXPORT_FROM_APP __declspec(dllexport)
#       endif
#   endif
#   define UDP_KEYSTONE_DLL_LOCAL
#elif __GNUC__ >= 4
#   define VCA_ALMA_EXPORT_FROM_DLL __attribute__((visibility ("default")))
#   define VCA_ALMA_EXPORT_FROM_APP __attribute__((visibility ("default")))
#   define VCA_ALMA_LOCAL           __attribute__((visibility ("hidden")))
#else
#   define VCA_ALMA_EXPORT_FROM_DLL
#   define VCA_ALMA_EXPORT_FROM_APP
#   define VCA_ALMA_LOCAL    
#endif

/** @brief Image formats
 *  These types define image formats that are supported by the VCA ALMA interface.  Images are assumed to be stored top-down with no additional row padding.
 */
typedef enum AlmaImageFormat
{
	ALMA_IMAGE_BGRA_8888 = 0, /**< BGRA format with 32 bits per pixel.  Image pixel data has the blue, green, red and alpha components in that order in memory.  The alpha component will be ignored. */
	ALMA_IMAGE_BGR_888 = 1    /**< Packed BGR format with 24 bits per pixel.  Image pixel data has blue, green and red components in that order in memory.*/

} AlmaImageFormat;

