#include "opencv2/opencv.hpp"
#include <stdlib.h>
#include <sys/time.h>
#include "wheelchairtracker.h"

using namespace cv;

int64_t current_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL);
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000;
    return milliseconds;
}

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		std::cerr << "usage: " << argv[0] << " <path to video>" << std::endl;
		std::cerr << "detects wheelchairs from the video stream given by the parameter" << std::endl;
		exit(-1);
	}

	VideoCapture capture(argv[1]);
	if (!capture.isOpened())
	{
		std::cerr << "failed to open the input video" << std::endl;
		exit(-1);
	}

	createWheelchairTracker();
	std::cout << "wheelchair tracker created" << std::endl;

	uint64_t frame_number = 0;
	while(true)
	{
		Mat frame;
		capture >> frame;
		uint8_t *data = frame.data;

		char output_buffer[5000];
		int32_t bytes_written;

		processWheelchairTracker(data, frame.cols, frame.rows, ALMA_IMAGE_BGR_888,
			current_timestamp(), frame_number++, output_buffer, 5000,
			&bytes_written);

		std::cout << output_buffer << std::endl;

		if(waitKey(30) >= 0) break;
	}

	destroyWheelchairTracker();
	return 0;
}
