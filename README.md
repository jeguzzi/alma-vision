# README

## Host installtion

### Dependencies

```bash
sudo apt-get install libcurl4-openssl-dev libssh2-1-dev libssl-dev libopencv-dev libqtcore4 libqtgui4
```

### Clone the repository

```bash
git clone https://jeguzzi@bitbucket.org/jeguzzi/alma-vision.git
cd alma-vision
git submodule update --init --recursive
```
### Build

```bash
mkdir build
cd build
cmake ..
make
```
### Configure

Edit launch.bash

### Run

```bash
cd <repository>
launch.bash
```

## Docker

### Requisite

Install docker, see https://docs.docker.com/engine/installation/linux/ubuntulinux/

### Clone the repository

```bash
git clone https://jeguzzi@bitbucket.org/jeguzzi/alma-vision.git
cd alma-vision
git submodule update --init --recursive
```

### Configure

Edit launch-docker.bash

### Run

```bash
cd <repository>
launch-docker.bash
```
