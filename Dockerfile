FROM ubuntu:latest
MAINTAINER Jerome Guzzi "jerome@idsia.ch"

RUN apt-get update && apt-get install -y \
   build-essential \
   libcurl4-openssl-dev \
   libssh2-1-dev \
   libssl-dev \
   && rm -rf /var/lib/apt/lists/*

 RUN apt-get update && apt-get install -y \
    cmake \
    libopencv-dev \
    libqtcore4 \
    libqtgui4 \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /src

COPY main.cpp /src/main.cpp
COPY calib.xml /src/calib.xml
COPY CMakeLists.txt /src/CMakeLists.txt
COPY almacrowddensity /src/almacrowddensity
COPY almawheelchair /src/almawheelchair
COPY cpr /src/cpr

RUN /bin/bash -c "pushd /src; mkdir build; pushd build; cmake ..; make; make install; popd; popd"
